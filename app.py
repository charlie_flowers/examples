#!/usr/bin/env python3
import aws_cdk as cdk

from test.test_stack import TestStack

app = cdk.App()

# Pass environments variable to this stack so we don't need to import
TestStack(app, "TestStack")

app.synth()
